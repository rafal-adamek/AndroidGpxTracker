package test.com.gpxtracker.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

public class Gpx {
    private List<LatLng> coordinatesList;
    private LatLngBounds.Builder mapBoundsBuilder;

    public Gpx() {
        this.coordinatesList = new ArrayList<>();
        this.mapBoundsBuilder = new LatLngBounds.Builder();
    }

    public void addCoords(LatLng coords) {
        this.coordinatesList.add(coords);
        this.mapBoundsBuilder.include(coords);
    }

    public List<LatLng> getCoordinatesList() {
        return this.coordinatesList;
    }

    public LatLngBounds getBounds() {
        return mapBoundsBuilder.build();
    }
}
