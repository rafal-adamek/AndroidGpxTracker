package test.com.gpxtracker;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;
import test.com.gpxtracker.model.Gpx;

public class MapPresenter implements MapContract.MapPresenter {

    private MapContract.MapView view;

    private InputStream resourceInputStream;

    private CompositeDisposable compositeDisposable;

    private String gpxResource = "";

    MapPresenter(MapContract.MapView view) {
        this.view = view;
    }

    @Override
    public void init() {
        this.compositeDisposable = new CompositeDisposable();

        this.view.initializeGpxDialog();
    }

    @Override
    public void setGpxResource(String gpxResource) {
        this.gpxResource = gpxResource;
    }

    @Override
    public void fetchGpx() {
        if(!this.gpxResource.equals("")) this.fetchGpx(this.gpxResource);
    }

    @Override
    public void fetchGpx(String gpxResource) {

        try {
            resourceInputStream = view.getFile(gpxResource);
        } catch (Resources.NotFoundException e) {
            view.showError(R.string.generic_error_title, R.string.generic_error_message);
            return;
        }

        this.gpxResource = gpxResource;

        Single<Gpx> gpxParserSubscriber = Single.create(subscriber -> {
            try {
                Gpx gpx = decodeGpx(resourceInputStream);
                subscriber.onSuccess(gpx);
            } catch (Throwable e) {
                subscriber.onError(Exceptions.propagate(e));
            }
        });

        gpxParserSubscriber
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Gpx>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull Gpx gpx) {
                        PolylineOptions polylineOptions = new PolylineOptions()
                                .color(Color.RED)
                                .addAll(gpx.getCoordinatesList())
                                .width(15);
                        view.drawPolyline(polylineOptions, gpx.getBounds());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.showError(R.string.generic_error_title, R.string.generic_error_message);
                    }
                });

    }

    private Gpx decodeGpx(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {

        Gpx gpx = new Gpx();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Document document = documentBuilderFactory.newDocumentBuilder().parse(inputStream);
        Element elementRoot = document.getDocumentElement();

        NodeList nodelist = elementRoot.getElementsByTagName("trkpt");

        for (int i = 0; i < nodelist.getLength(); i++) {
            Node node = nodelist.item(i);
            NamedNodeMap attributes = node.getAttributes();

            Double newLatitude = Double.parseDouble(attributes.getNamedItem("lat").getTextContent());
            Double newLongitude = Double.parseDouble(attributes.getNamedItem("lon").getTextContent());
            LatLng newLocation = new LatLng(newLatitude, newLongitude);

            gpx.addCoords(newLocation);
        }

        inputStream.close();

        return gpx;
    }

    @Override
    public void fabButtonClicked() {
        view.showGpxPicker();
    }

    @Override
    public Bundle saveData(Bundle outData) {
        if(!this.gpxResource.equals(""))  {
            outData.putString(MapActivity.GPX_KEY, this.gpxResource);
        }
        return outData;
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
    }
}
