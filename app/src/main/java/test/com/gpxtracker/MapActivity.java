package test.com.gpxtracker;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import test.com.gpxtracker.databinding.ActivityMapBinding;
import test.com.gpxtracker.databinding.DialogGpxPickerBinding;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, MapContract.MapView {

    public static final String GPX_KEY = "GPX_KEY";

    private GoogleMap map;
    private MapContract.MapPresenter presenter;
    private ActivityMapBinding root;
    private AlertDialog alertDialogGpx;

    //Lifecycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        root = DataBindingUtil.setContentView(this, R.layout.activity_map);

        this.presenter = new MapPresenter(this);

        this.presenter.init();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.f_map);
        mapFragment.getMapAsync(this);

        this.root.fabMain.setOnClickListener((View v) -> this.presenter.fabButtonClicked());

        if (savedInstanceState != null) {
            this.presenter.setGpxResource(savedInstanceState.getString(GPX_KEY, ""));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(this.presenter.saveData(outState));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.presenter.onDestroy();
    }

    //Implementation

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        this.presenter.fetchGpx();
    }

    @Override
    public InputStream getFile(String resourceName) throws Resources.NotFoundException {
        return getResources().openRawResource(getResources().getIdentifier(resourceName, "raw", getPackageName()));
    }

    @Override
    public void drawPolyline(PolylineOptions polylineOptions, LatLngBounds bounds) {
        map.clear();

        map.addPolyline(polylineOptions);

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10);

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        map.animateCamera(cu);
    }

    @Override
    public void showError(@StringRes int title, @StringRes int message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(message);
        alertDialog.setTitle(title);
        alertDialog.show();
    }

    @Override
    public void initializeGpxDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        DialogGpxPickerBinding dialogView = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_gpx_picker, null, false);
        alertDialogBuilder.setView(dialogView.getRoot());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);

        List<String> items = new ArrayList<>();

        for (int i = 0; i < R.raw.class.getFields().length; i++)
            items.add(R.raw.class.getFields()[i].getName());

        adapter.addAll(items);

        dialogView.sRaw.setAdapter(adapter);
        alertDialogBuilder.setPositiveButton(R.string.dialog_gpx_ok, (dialogInterface, i) -> this.presenter.fetchGpx(dialogView.sRaw.getSelectedItem().toString()));

        alertDialogGpx = alertDialogBuilder.create();
    }


    @Override
    public void showGpxPicker() {
        this.alertDialogGpx.show();
    }
}
