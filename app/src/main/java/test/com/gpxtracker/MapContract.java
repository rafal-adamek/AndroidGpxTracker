package test.com.gpxtracker;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.InputStream;

public class MapContract {

    public interface MapView {
        InputStream getFile(String resourceName);
        void showError(@StringRes int title, @StringRes int message);
        void drawPolyline(PolylineOptions options, LatLngBounds bounds);
        void initializeGpxDialog();
        void showGpxPicker();
    }

    public interface MapPresenter {
        void setGpxResource(String gpxResource);
        void fetchGpx();
        void fetchGpx(String name);
        void fabButtonClicked();
        Bundle saveData(Bundle outData);
        void init();
        void onDestroy();
    }
}
